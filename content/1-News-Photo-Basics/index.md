# 1. News Photo Basics

When taking photographs of a news event, remember the viewer can’t see the totality of the scene as you can. They can’t know what you don’t show them in the photograph. You need to carefully build your image to best capture the scene. The world can look very different in a photograph. Strong photojournalists capture not just the **drama** and **action** of an event in their images. They also include visual elements that provide context as it relates to physical location and the causes of the event being photographed. This requires you to ‘think visually,’ which can be a challenge for even the best journalists.

Photojournalists become very good at seeing a scene as the component parts of a good photograph. They organize those parts in the frame, to tell a well-composed, visual story. They do this through **position**, **timing** and **angle-of-view**. What is excluded from the frame because it is visually confusing or will make the narrative point of the image unclear can be as important as what is included in the frame.

---

The backbone or fundamental technical principle of good composition is the **rule of thirds** (see illustration on the next page). The rule of thirds is a medieval, European concept for composing pictures. It breaks down the frame (or canvas) into nine equal rectangles, sometimes called the grid. Many cameras from smartphones on up will allow you to see the rule of thirds grid in the display screen.

![rule of thirds](images/PN_2.jpg)

Good photographers will compose along the intersections of lines in the frame and work to use each of the nine rectangles to tell their story. Amateurs tend to plop the subject in the middle of the frame and ignore much of the remaining space rather than use it to convey more information and make a more compelling image.

---

Another important principle for good composition is called **working the layers**. A photograph is a rectangle, a two-dimensional space into which we compress a visual idea of the three dimensional world. Creating distinct layers in your frame: foreground, middleground and background (in practice, often just foreground and background) helps you tell clearer, more complex visual stories with your photography. For example, you might choose to place your subject, a farmer whose fields are barren because of drought, frame left in the foreground and his dried up empty field in the background. This will give the image a clear subject layer (foreground) and context (background layer).

![second image](images/PN_3.jpg)

In a well-composed photograph, important shapes or information complete themselves in the frame. The cook’s hands are not cut off awkwardly.The personnel carrier that provides context to the picture in the background layer isn’t chopped in half at the edge of the frame. The ball isn’t cut in half after being kicked on goal by the forward. Photographers call this **working the edges**. On a more graphic level, eye-catching or graphically powerful shapes like door frames or trees or objects of strong color are used to define the edge of the frame by allowing them to resolve within it.

---

In news photography you want to move away from posed, static images. Human actions tells story. Think peak action – the moment at an event when the subject is most animated or demonstrative. Remember: interaction and reaction to an event can be a  s powerful as the event itself.

![third image](images/PN_4.jpg)

Perhaps the biggest challenge to better photography is learning to think visually. This is about more than just composition. It is also about investing yourself in taking better photographs. Many reporters who take photos consider the photography as an afterthought. They report their story and snap a couple pictures at the end. Good photography takes time. In the beginning you need to practice. You need to plan for it and think through how you will get the kinds of images that really capture the essence of a story and move viewers emotionally.

---
