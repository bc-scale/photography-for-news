# 2. In the Field

Some stories are simply more visual than others. Stories about events that happened in the past are hard to visualize -- a story about a store that was robbed a week ago, for example. Or, a story about a change in the language of the constitution or about financial interest rates. These stories are tough to photograph. Good photographers will illustrate the human impact of these types of stories. If nothing else works, a portrait of the main character or the scene of the event.

Your images provide a visual summary of the story. Portray the important elements of the story. Think subject and context. Think peak action. What will people need to see to understand the story from the images? Your images must include those elements. If it is a story about the effects of drought on a family of farmers, we need to see the family and the barren fields in the same picture -- that combination tells the story visually.

Already in this module you have been given tools to help you compose an image to include the important visual elements of your story. Think about layers -- foreground and background. Think about the rule-of-thirds. Apply both of these principles to how you build or organize the content of your image to tell a story.

---
![](images/PN_5.jpg)

Position and timing are important tools when telling a visual story. Good photographers anticipate action and position themselves to make the most of it.

![](images/PN_6.jpg)

Don’t be afraid to move around as a photographer. Consider again the example of a family whose farm has been destroyed by drought.

![](images/PN_7.jpg)

Dramatic moments are key to good images. For example, an image of an anxious-looking farmer with his family and a barren field in the background.

![](images/PN_8.jpg)

Sometimes it is hard to get the whole story in a single image. If you think you might want to use more than one photo, shoot the photos you want and sequence them as a story.

![](images/PN_9.jpg)

Each image should illustrate a different aspect of the scene so that the viewer gets a sense of the totality of the event. Try and shoot a variety of images -- some wide shots, some close-ups or detail shots, and maybe even a portrait. Try and sequence the images in a logical order. Chronology is often the most straightforward way to order images. However, it is always a good idea to put your most dramatic images first.

---
