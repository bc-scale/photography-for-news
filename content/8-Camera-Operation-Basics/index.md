# 8. Camera Operation Basics

A big part of understanding how a camera works and how to use it is understanding what it does not do well compared to the human eye. Your eye can see details in extreme contrasty places -- highlights on a snow-covered mountain top, for example, and dark shadows in a cave entrance in the side of that same mountain. Digital cameras cannot estimate exposure well. Images often appear too bright or, conversely, too dark.

Your eye is quick to focus on the singular thing of interest in your field of view. A digital camera must be told where to focus, especially in low light, and can be slow to do it.

A camera must be told to freeze motion and struggles to do it with fast-moving objects, especially in low light. It takes much more light to record an image with a digital camera than for your eye to see it.

The camera app will vary from Android-based mobile devices to another. All should work with StoryMaker. The StoryMaker tutorials will walk you through basic camera app operation as well as reinforce some of the compositional material here.

The biggest thing to remember for many journalists with mobile cameras is that they are limited in what they can do compared to larger point-and- shoots and certainly dSLRs. The most significant problem is the lens and aperture within it. Because the lens is so small in a smartphone camera the maximum aperture size is also small, dramatically reducing the amount of light that strikes the sensor. This makes them tough to use in low light or with fast-moving objects.

Understanding these limitations and working within them is the key to getting the most out of your smartphone camera. Remember, if you have a smartphone and you need photos for your story, plan your coverage around time of day or lighting conditions if possible. Additionally, while the image of ambulances speeding by might be a great one for your story, you may opt to also take a more static picture of the front the hospital to make sure you have a usable image in case the ambulance photo doesn’t work out.

A digital camera is very simple. It has a digital sensor that collects light, a shutter that controls how long the sensor is exposed to light and a lens which controls the amount of light that strikes the sensor. Here is a brief explanation of each.

---
