# 3. Collecting Captions and Keyword Information

Good captions and keywords are essential to good photojournalism. Captions help explain to the viewer what they see in the image. Images themselves are not searchable on social sites such as Flickr and YouTube. Good keyword are essential to people being able to find your images. Captions and keywords together help viewers and news organizations validate what they see in an image. Adding relevant hashtags to your image’s caption can help you build audience on social sites.

---

## Captions

Here are some guidelines for writing captions:

* Clearly identify the people and location that appear in the photo. Professional titles should be included as well as the formal name of the location. SPELL NAMES CORRECTLY (check against the spellings in the article if necessary) For photographs of more than one person, identifications typically go from left to right. Written in Arabic, you may consider identifications from right to left. In the case of large groups, identifications of only notable people may be required and sometimes no I.D.s are required at all. Your publication should establish a standard for its photographers.

* Include the date the photograph was taken. This is essential information for a news publication. The more recent a photo is, the better. If an archive photograph or photograph taken prior to the event being illustrated is used, the caption should make clear that it is a “file photo.”

* Provide some context or background so the reader can understand the news value of the photograph. A sentence or two is usually sufficient.

* Photo captions should be written in complete sentences and in the
present tense. The present tense gives the image a sense of immediacy.

* It is not always logical to write the entire caption in the present tense.

* Often the first sentence is written in the present tense and following sentences are not.

---

## Keywords

Keywords should relate the image to broader topical themes of interest to viewers. Ideally keywords should not repeat words that appear in the caption, but because search engines view and rank text in complicated ways, there are some exceptions. These might include: geographic information, names of significant public figures, or other proper nouns.

Here are some guidelines for keyword writing:

* Use between five and 10 keywords.
* Write complete words. Avoid abbreviations.
* Order the keywords in terms of importance or likely search choice.
* Select clear, common descriptors: ‘environmental’ rather than ‘green.’
* Include broad descriptors for specific names used in the caption. For example, if ‘the Indira Gandhi Institute of Child Medicine’ appears in the caption use ‘children’ and ‘hospital’ as keywords.
* Include geographic location.
* Include important proper names.

---
